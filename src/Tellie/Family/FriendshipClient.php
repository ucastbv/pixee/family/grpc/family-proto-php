<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Tellie\Family;

/**
 */
class FriendshipClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Tellie\Family\FriendshipCreatedRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyCreated(\Tellie\Family\FriendshipCreatedRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.Friendship/NotifyCreated',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Tellie\Family\FriendshipUpdatedRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyUpdated(\Tellie\Family\FriendshipUpdatedRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.Friendship/NotifyUpdated',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Tellie\Family\FriendshipRemovedRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyRemoved(\Tellie\Family\FriendshipRemovedRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.Friendship/NotifyRemoved',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Tellie\Family\FriendshipReactivatedRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyReactivated(\Tellie\Family\FriendshipReactivatedRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.Friendship/NotifyReactivated',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

}
