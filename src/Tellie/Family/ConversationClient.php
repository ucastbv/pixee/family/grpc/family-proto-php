<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Tellie\Family;

/**
 */
class ConversationClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Tellie\Family\MessageSentRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyMessageSent(\Tellie\Family\MessageSentRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.Conversation/NotifyMessageSent',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

}
