<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Tellie\Family;

/**
 */
class EndUserClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Tellie\Family\EndUserDeactivatedRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyDeactivatedEndUserAndTheirFriends(\Tellie\Family\EndUserDeactivatedRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.EndUser/NotifyDeactivatedEndUserAndTheirFriends',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Tellie\Family\EndUserReactivatedRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyReactivatedEndUserAndTheirFriends(\Tellie\Family\EndUserReactivatedRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.EndUser/NotifyReactivatedEndUserAndTheirFriends',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Tellie\Family\SettingsChangedRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyEndUserOfChangedSettings(\Tellie\Family\SettingsChangedRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.EndUser/NotifyEndUserOfChangedSettings',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

}
