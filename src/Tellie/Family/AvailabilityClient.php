<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Tellie\Family;

/**
 * The Availability service definitions.
 */
class AvailabilityClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Tellie\Family\ChangeAvailabilityRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyUserDoNotDisturbEnabled(\Tellie\Family\ChangeAvailabilityRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.Availability/NotifyUserDoNotDisturbEnabled',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Tellie\Family\ChangeAvailabilityRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function NotifyUserDoNotDisturbDisabled(\Tellie\Family\ChangeAvailabilityRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/tellie.family.Availability/NotifyUserDoNotDisturbDisabled',
        $argument,
        ['\Google\Protobuf\GPBEmpty', 'decode'],
        $metadata, $options);
    }

}
